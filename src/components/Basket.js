import React, {PropTypes, Component} from 'react'
import BasketItems from '../components/BasketItems'
import Sum from '../components/Sum'
import    {Link}    from    'react-router'

export default class Basket extends Component {

    render() {
        let basketItems = this.props.data;
        console.log(basketItems);
        let index = getIndexBasketItem(basketItems, this.props.name);
        if (index > -1) {
            basketItems[index].number = basketItems[index].number + 1;
        } else {
            if (this.props.name != "") {
                let basketItem = {};
                basketItem.name = this.props.name;
                basketItem.price = this.props.price;
                basketItem.number = 1;
                basketItems.push(basketItem);
            }
        }

        return (
            <div className="basket">
                <table className="basket_table">
                    <tbody>
                    <tr>
                        <td>
                            <p className="title_basket">Basket</p>
                            <BasketItems data={basketItems}/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Sum data={basketItems}/>
                            <div className="order_sum">
                                <Link className="button" to='/confirm'>Заказать</Link>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

Basket.propTypes = {
    data: React.PropTypes.array.isRequired,
    name: React.PropTypes.string.isRequired,
    price: React.PropTypes.number.isRequired
};

function getIndexBasketItem(basketItems, itemName) {
    for (let i = 0; i < basketItems.length; i++) {
        if (basketItems[i].name == itemName) {
            return i;
        }
    }
    return -1;
}