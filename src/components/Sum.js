import React, {PropTypes, Component} from 'react'
import DeliveryCondition from '../components/DeliveryCondition'

export default class Sum extends Component {

    render() {
        let data = this.props.data;
        let sumCount = 0;
        data.map(function (item) {
            sumCount = sumCount + item.price * item.number;
        });
        return (
            <div className="sum">
                Всего: {sumCount}
                <DeliveryCondition sum={sumCount}/>
            </div>
        );
    }
}

Sum.propTypes = {
    data: React.PropTypes.array.isRequired
};
