import React, {PropTypes, Component} from 'react'

export default class ListItems extends Component {
    onBtnClickHandler (item) {
        this.props.setItems(item);
    }
    render() {
        let data = this.props.data;
        let self = this;
        console.log(data);
        let items = data.map(function (item, index) {
            return (
                <div className="cell_item" key={index}>
                    <p className="name_and_price">{item.name + ", " + item.price + "p"}</p>
                    <button onClick={() => self.onBtnClickHandler(item)}>Добавить в корзину</button>
                </div>
            )
        });
        return (
            <div className="list_items">
                {items}
            </div>
        );
    }
}

ListItems.propTypes = {
    data: React.PropTypes.array.isRequired,
    setItems: React.PropTypes.func.isRequired
};
