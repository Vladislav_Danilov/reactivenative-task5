import React, {PropTypes, Component} from 'react'
import    {Link}    from    'react-router'

export default class Root extends Component {

    render() {
        return (
            <div>
                Страница не найдена.
                <Link to='/'>Вернуться на главную</Link>
            </div>
        );
    }
}
