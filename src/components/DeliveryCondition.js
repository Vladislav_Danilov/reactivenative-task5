import React, {PropTypes, Component} from 'react'

export default class DeliveryCondition extends Component {

    render() {
        let sum = this.props.sum;
        if (sum > 1000) {
            return (
                <div className="delivery_condition">
                    Доставка: бесплатно
                </div>
            )
        }
        if (sum > 500) {
            return (
                <div className="delivery_condition">
                    Доставка: 100p
                </div>
            )
        }
        return (
            <div className="delivery_condition">
                Доставка: 200p
            </div>
        )
    }
}

DeliveryCondition.propTypes = {
    sum: React.PropTypes.number.isRequired
};
