import React, {PropTypes, Component} from 'react'

export default class Root extends Component {

    render() {
        return (
            <div>
                <header>Shop</header>
                <article>
                    {this.props.children}
                </article>
                <footer>ReactJS: task four &copy;</footer>
            </div>
        );
    }
}
