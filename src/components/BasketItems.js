import React, {PropTypes, Component} from 'react'

export default class BasketItems extends Component {

    render() {
        let data = this.props.data;
        let items = data.map(function (item, index) {
            console.log(item);
            return (
                <div className="basket_item" key={index}>
                    <p className="name_and_price">{item.name + " x " + item.price * item.number}</p>
                </div>
            )
        });
        return (
            <div className="list_basket_items">
                {items}
            </div>
        );
    }
}

BasketItems.propTypes = {
    data: React.PropTypes.array.isRequired
};