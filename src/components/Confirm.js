import React, {PropTypes, Component} from 'react'
import BasketItems from '../components/BasketItems'
import Sum from '../components/Sum'
import    {connect}    from    'react-redux'

class Confirm extends Component {

    render() {
        const {basket} = this.props;
        return (
            <div className="confirm_container">
                <BasketItems data={basket.basketItems}/>
                <Sum data={basket.basketItems}/>
                <a className="button">Подтвердить</a>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        basket: state.basket
    }
}

export default connect(mapStateToProps)(Confirm)
