const initialState = {
    nameItem: "",
    price: 0,
    basketItems: []
};

export default function basket(state = initialState, action) {
    switch (action.type) {
        case 'ADD_ITEM':
            return {...state, price: action.payload.price, nameItem: action.payload.name};

        default:
            return state;
    }
}