const initialState = {
    items: [
        {
            name: 'Товар 1',
            price: 100
        },
        {
            name: 'Товар 2',
            price: 600
        },
        {
            name: 'Товар 3',
            price: 200
        },
        {
            name: 'Товар 4',
            price: 300
        },
        {
            name: 'Товар 5',
            price: 500
        }
    ]
};

export default function items(state = initialState) {
    return state
}