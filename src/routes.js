import React from 'react'
import {Route, IndexRoute} from 'react-router'

import App from './containers/App'
import Root from './components/Root'
import NotFound from './components/NotFound'
import Confirm from './components/Confirm'

export const routes = (
    <Route path='/' component={Root}>
        <IndexRoute component={App}/>
        <Route path="confirm" component={Confirm}/>
        <Route path="*" component={NotFound}/>
    </Route>
);
