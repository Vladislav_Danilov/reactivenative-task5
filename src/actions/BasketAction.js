export function setItem(item) {

    return {
        type: 'ADD_ITEM',
        payload: item
    }
}