import React, {Component} from 'react'
import {connect} from 'react-redux'
import ListItems from '../components/ListItems'
import Basket from '../components/Basket'
import {bindActionCreators} from 'redux'
import * as basketAction from '../actions/BasketAction'

class App extends Component {
    render() {
        const {items, basket} = this.props;
        const {setItem} = this.props.basketAction;

        return (
            <table className="shop_details_container">
                <tbody>
                <tr>
                    <td className="list_items_container">
                        <ListItems data={items.items} setItems={setItem}/>
                    </td>
                    <td className="basket_container">
                        <Basket data={basket.basketItems} name={basket.nameItem} price={basket.price}/>
                    </td>
                </tr>
                </tbody>
            </table>
        )
    }
}

function mapStateToProps(state) {
    return {
        items: state.items,
        basket: state.basket
    }
}

function mapDispatchToProps(dispatch) {
    return {
        basketAction: bindActionCreators(basketAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);